# SUSY Workshop '23 Tutorials: Statistical Analysis and Reinterpretations

**Disclaimer:** This is more of a software that a statistics tutorial.

In this part of the tutorial we will go through the statistical interpretation of our example analysis. The final result would be the CLs value, which is a _p_-value like quantity assessing the plausibility of a hypothesis, in this case the _signal_+_background_ hyppothesis.  

## Model-dependent (aka Exclusion) fit:

The model-dependent fit is a frequentist fit performed to study a specific signal model. It is also called _exclusion_ fit, as it is used in cases that there is no significant excess of data events above the estimated SM background in the SRs (fact that is known already by yesterday's _background-only_ fit) in order to exclude or not a specific signal model hypotesis. Alternatively, in the case of an actuall data excess, the model-dependent fit could be used to measure the signal strength of a signal model.

In this fit scenario, the likelihood function includes terms for both the signal process under investigation and all ther relevant background and is fit in both the analysis CRs and SRs. For example:
$$
L  = \prod_{i\in\text{SR}} \mathcal{P}(n_i|\lambda_i(\mu_\text{sig},\boldsymbol{\mu},\boldsymbol{\theta})) \times \prod_{i\in\text{CR}} \mathcal{P}(n_i|\lambda_i(\mu_\text{sig},\boldsymbol{\mu},\boldsymbol{\theta})) \times C(\boldsymbol{\theta}^\mathrm{0},\boldsymbol{\theta}),
$$
where $n$ is the number of observed events, $\lambda$ the number of expected events, $\boldsymbol{\mu}$ the backgrounds normalization factors and $\boldsymbol{\theta}$ the nuisance parameters of the fit that parametrise the analysis systematic uncertainties.

From this fit the normalization factor/signal strength ($\mu_{\mathrm{sig}}$) is calculated for the signal under investigation, along with the normalization factors of the relevant backgrounds. 

In an analysis, during the exclusion fit, a grid of signal models, usually with different mass assumptions, is considered and a fit is performed individually for each one of the signal models of the grid. The results allow an exclusion contour to be placed in the mass plane considered. In particular, the signal strength is used as the parameter of interest (POI) in each test and the resulted CLs indicates if a model is considered excluded or not. For an exclusion limit at the 95\% Confidence Level (CL), signal models are considered excluded when CLs < 0.05. Finally, we can interpolate among all the different CLs values for each signal model and draw contour at 0.05. This is how we get our usual limit plots, such the one bellow from [JHEP 12 (2019) 060](https://link.springer.com/article/10.1007/JHEP12(2019)060):

![alt text](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/SUSY-2018-31/fig_08a.png)

## Further information

Couple of my favourite readings on the topic:  
1. K. Cranmer, _Practical Statistics for the LHC_, 2015 ([arXiv:1503.07622](https://doi.org/10.48550/arXiv.1503.07622)).
2. O. Behnke, K. Kroninger, G. Schott, T. Schorner-Sadenius, _Data Analysis in High Energy Physics: A Practical Guide to Statistical Methods_, Wiley‐VCH, 2013, ([10.1002/9783527653416](https://onlinelibrary.wiley.com/doi/book/10.1002/9783527653416))

In terms of software, we will stay in `python` environment and use two main libraries:  
1. `pyhf`: https://github.com/scikit-hep/pyhf
2. `cabinetry`: https://github.com/scikit-hep/cabinetry

A realistic analysis statistical interpretation setup can be found in: https://gitlab.cern.ch/ekourlit/pyfits/

## Running the tutorial

This tutorial is meant to be run on SWAN (Service for Web-based ANalysis). To clone this repository into a SWAN project for you, please click on the button below:

[![Open in SWAN][image]][hyperlink]

[image]: img/open_in_swan.svg

[hyperlink]: https://swan-k8s.cern.ch/user-redirect/download?projurl=https://gitlab.cern.ch/ekourlit/susyworkshop23-tutorial-block3.git

In case you currently don't have a running session on SWAN you should get asked what software stack you want to use. Please select the "Bleeding Edge" release as we need one package in the tutorial ('cabinetry') which isn't contained in the official LCG releases. You can leave all other settings at the default values. If you already have a SWAN session runnint, please change the configuration to use the "Bleeding Edge" release, othwise parts of the tutorial will not work.

## Next: Result re-interpretation

In the next part of the tutorial we see how we can take an already existing analysis result and re-interpret it in the context of another BSM model.  
Please follow here: https://gitlab.cern.ch/lheinric/susyworkshop23-tutorial-block3-recast